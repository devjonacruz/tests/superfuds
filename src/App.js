import React, { Component } from "react";

import "bootstrap/dist/css/bootstrap.min.css";
import "./css/App.css";
import Header from "./components/common/header";
import Carousel from "./components/product";
import Cart from "./components/cart";

import Parent from "./components/Parent";

import { connect } from "react-redux";

const mapStateToProps = (state) => ({
  show: state.show,
});

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header></Header>
        <Carousel></Carousel>
        {this.props.show ? <Cart></Cart> : null}
        <Parent></Parent>
      </div>
    );
  }
}

export default connect(mapStateToProps)(App);
