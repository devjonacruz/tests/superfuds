import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faAngleLeft,
  faPlusCircle,
  faMinusCircle,
  faTrash,
} from "@fortawesome/free-solid-svg-icons";

import { connect } from "react-redux";
import { showCart } from "../../redux/actions";

const mapStateToProps = (state) => {
  const { products } = state;
  return { products };
};

class Main extends Component {
  handleCart = () => {
    // this.props.showCart(false);
    this.props.dispatch({ type: "INCREMENT_PARENT" });
    this.props.dispatch({ type: "SHOW_CART", payload: false });
  };

  listItems = (products) => {
    return products.map((product) => {
      return (
        <div className="my-3">
          <div className="d-flex align-items-center justify-content-start">
            <img src={product.image} alt="" width="150px" />
            <div>
              <p className="mb-0 fw-900">{product.title}</p>
              <p>
                x {product.units_sf} unids - {product.net_content} c/u
              </p>
              <p className="text-green">{product.supplier}</p>
            </div>
          </div>
          <div className="d-flex align-items-center justify-content-around p-4">
            <FontAwesomeIcon
              icon={faMinusCircle}
              className="text-green mr-2"
              size="2x"
            />
            1
            <FontAwesomeIcon
              icon={faPlusCircle}
              className="text-green mr-2 fs-24"
              size="2x"
            />
          </div>
          <div className="d-flex align-items-center justify-content-center">
            <span className="text-green mr-1">$</span> {product.price_real}
          </div>
          <div className="d-flex align-items-center justify-content-center">
            <FontAwesomeIcon
              icon={faTrash}
              className="text-green mr-2"
              size="lg"
            />
          </div>
        </div>
      );
    });
  };

  render() {
    const { products } = this.props;

    return (
      <div className="cart">
        <div className="cart-content">
          <div style={{ cursor: "pointer" }} onClick={this.handleCart}>
            <FontAwesomeIcon icon={faAngleLeft} className="text-green mr-2" />
            Volver a la tienda
          </div>
          <div className="cart-title d-flex align-items-center justify-content-between">
            <span>Carrito de compras</span>{" "}
            <span>
              <span className="text-green">3</span> items
            </span>
          </div>
          <div className="cart-items">
            <div>
              <div>Item</div>
              <div>Cantidad</div>
              <div>Precio</div>
              <div></div>
            </div>
            {products.length === 0 ? null : this.listItems(products)}
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Main);
