import React, { Component } from "react";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

import { connect } from "react-redux";
import { addToCart } from "../../redux/actions";

const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 5,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
};

class Main extends Component {
  state = {
    products: [],
  };

  _listItems = (products) => {
    return products.map((product) => {
      return (
        <div className="carousel-item-product-container">
          <div className="carousel-item-product">
            <div className="carousel-item-product-img text-center">
              <img src={product.image} alt="" />
            </div>
            <hr width="95%" />
            <div className="d-flex align-items-center justify-content-between">
              <span className="text-green">{product.supplier}</span>
              <span className="bg-green">{product.net_content}</span>
            </div>
            <p>
              {product.title.length < 30
                ? product.title
                : `${product.title.substring(0, 30)}...`}
            </p>
            <p className="d-flex align-items-center justify-content-start">
              <span>
                <span>$</span>
                <span>{product.price_real}</span>
              </span>
              <span>x {product.units_sf} unids</span>
            </p>
            <div className="tooltiptext">
              <div>Producto</div>
              <div>{product.sellos[0].name}</div>
            </div>
          </div>
          <div className="carousel-item-product-add-container">
            <div
              className="carousel-item-product-add pointer"
              onClick={() => this.handleAddCart(product)}
            >
              Agregar al carrito
            </div>
          </div>
        </div>
      );
    });
  };

  handleAddCart = (product) => {
    this.props.addToCart(product);
  };

  render() {
    let { products } = this.state;

    return (
      <Carousel
        swipeable={false}
        draggable={false}
        responsive={responsive}
        ssr={true} // means to render carousel on server-side.
        infinite={true}
        autoPlay={this.props.deviceType !== "mobile" ? true : false}
        autoPlaySpeed={1000}
        keyBoardControl={true}
        customTransition="all 1"
        transitionDuration={1000}
        removeArrowOnDeviceType={["tablet", "mobile"]}
        deviceType={this.props.deviceType}
        containerClass="carousel-container"
        itemClass="carousel-item-padding-40-px d-flex align-items-center justify-content-center"
      >
        {products.length === 0 ? (
          <div>Cargando productos...</div>
        ) : (
          this._listItems(products)
        )}
      </Carousel>
    );
  }

  componentDidMount() {
    fetch(
      "https://superfuds-assets.s3-sa-east-1.amazonaws.com/utils/product.json"
    )
      .then((res) => res.json())
      .then((products) => {
        this.setState({
          products,
        });
      });
  }
}

export default connect(null, { addToCart })(Main);
