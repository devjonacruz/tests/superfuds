export const addToCart = (content) => ({
  type: "ADD_TO_CART",
  payload: content,
});

export const removeToCart = (content) => ({
  type: "REMOVE_TO_CART",
  payload: content,
});

export const showCart = (content) => ({
  type: "SHOW_CART",
  payload: content,
});
