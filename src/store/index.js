import { createStore } from "redux";

function todos(state = [], action) {
  switch (action.type) {
    case "ADD_TO_CART":
      state.products.push(action.payload);
      return state;

    case "SHOW_CART":
      state.show = action.payload;
      return state;

    case "INCREMENT_PARENT":
      return { ...state, parentCounter: state.parentCounter + 1 };

    case "INCREMENT_CHILD":
      return { ...state, childCounter: state.childCounter + 1 };

    default:
      return state;
  }
}

const store = createStore(
  todos,
  {
    products: [],
    show: false,
    parentCounter: 0,
    childCounter: 0,
  },
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
